const fs = require("fs");
const path = require("path");

function creatingDirectoryWithJsonFilesandRemovingThem(
  dirName,
  number,
  callback
) {
  if (
    arguments.length < 3 ||
    typeof dirName !== "string" ||
    typeof number !== "number"
  ) {
    throw new Error("Please follow the input format");
  }
  number = Math.floor(number);
  try {
    fs.mkdir(path.join(__dirname, dirName), (err) => {
      if (err) {
        throw new Error("unable to create directory");
      }
    });
    callback(null, "New directory Created Successfully");

    for (let iterator = 1; iterator <= number; iterator++) {
      let filename = path.join(__dirname, dirName, `${iterator}.json`);

      fs.writeFile(filename, "utf-8", (err) => {
        if (err) {
          throw new Error("unable to create the file");
        }
      });
    }

    callback(null, `${number} .JSON files are created`);

    setTimeout(() => {
      for (let iterator = 1; iterator <= number; iterator++) {
        let filename = path.join(__dirname, dirName, `${iterator}.json`);

        fs.unlink(filename, (err) => {
          if (err) {
            throw new Error('unable to find file location');
          }
        });
      }
      callback(null, `${number} .Json files are deleted`);
    }, 4000);
  } catch (err) {
    callback(err);
  }
}

module.exports = creatingDirectoryWithJsonFilesandRemovingThem;
